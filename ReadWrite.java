import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Clase para el manejo de entrada y salida básica por consola.
 */
public class ReadWrite {

    private Scanner tc;

    /**
     * Constructor
     */
    public ReadWrite() {
        this.tc = new Scanner(System.in);
    }

    /**
     * Método que imprime en pantalla el mensaje con salto de línea.
     * 
     * @param s: Cadena a imprimir
     */
    public void println(String s) {
        System.out.println(s);
    }

    /**
     * Método que imprime en pantalla el mensaje.
     * 
     * @param s: Cadena a imprimir
     */
    public void print(String s) {
        System.out.print(s);
    }

    /**
     * Función que captura un tipo int. La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @return: Tipo int capturado
     */
    public int readInt() {
        int p = 0;
        boolean sw = false;
        do {
            try {
                p = this.tc.nextInt();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo int. La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @param s: Cadena que se imprime en modo de mensaje.
     * @return: Tipo int capturado
     */
    public int readInt(String s) {
        int p = 0;
        boolean sw = false;
        do {
            try {
                println(s);
                p = this.tc.nextInt();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo long La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @return: Tipo long capturado
     */
    public long readLong() {
        long p = 0;
        boolean sw = false;
        do {
            try {
                p = this.tc.nextLong();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo long La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @param s: Cadena que se imprime en modo de mensaje.
     * @return: Tipo long capturado
     */
    public long readLong(String s) {
        long p = 0;
        boolean sw = false;
        do {
            try {
                println(s);
                p = this.tc.nextLong();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo float. La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @return: Tipo float capturado
     */
    public float readFloat() {
        float p = 0;
        boolean sw = false;
        do {
            try {
                p = this.tc.nextFloat();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo float. La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @param s: Cadena que se imprime en modo de mensaje.
     * @return: Tipo float capturado
     */
    public float readFloat(String s) {
        float p = 0;
        boolean sw = false;
        do {
            try {
                println(s);
                p = this.tc.nextFloat();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo double. La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @return: Tipo double capturado
     */
    public double readDouble() {
        double p = 0;
        boolean sw = false;
        do {
            try {
                p = this.tc.nextDouble();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

    /**
     * Función que captura un tipo double. La función no deja avanzar hasta que se
     * ingrese un valor válido.
     * 
     * @param s: Cadena que se imprime en modo de mensaje.
     * @return: Tipo double capturado
     */
    public double readDouble(String s) {
        double p = 0;
        boolean sw = false;
        do {
            try {
                println(s);
                p = this.tc.nextDouble();
                sw = true;
            } catch (InputMismatchException e) {
                this.tc.next();
                continue;
            }
        } while (!sw);
        return p;
    }

}